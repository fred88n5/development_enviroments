const request = require('supertest');
const { server, serverInstance } = require('../server');
const { db, close } = require('../db');


let testUser = {
  email: 'test@example.com',
  password: 'testPassword'
};

let usersCollection;

describe('User Endpoints', () => {
  beforeAll(async () => {
    const dbConnection = await db();
    usersCollection = dbConnection.users;
  });

  it('should create a new user', async () => {
    const res = await request(server)
      .post('/api/signup')
      .send(testUser)
      .expect(201);
    expect(res.text).toEqual('User created successfully');
  });

  it('should not create a user with the same email', async () => {
    const res = await request(server)
      .post('/api/signup')
      .send(testUser)
      .expect(400);
    expect(res.text).toEqual('A user with this email already exists. Please use a different email.');
  });

  it('should login the user', async () => {
    const res = await request(server)
      .post('/api/login')
      .send(testUser)
      .expect(200);
    expect(res.text).toEqual('Logged in successfully');
  });

  it('should not login with invalid password', async () => {
    const res = await request(server)
      .post('/api/login')
      .send({
        email: testUser.email,
        password: 'wrongPassword'
      })
      .expect(400);
    expect(res.text).toEqual('Invalid email or password');
  });

  it('should fetch all users', async () => {
    const res = await request(server)
      .get('/api/users')
      .expect(200);
    expect(res.body).toBeInstanceOf(Array);
  });

  it('should delete the test user', async () => {
    const res = await request(server)
      .delete(`/api/users/${testUser.email}`)
      .expect(200);
    expect(res.text).toEqual('User deleted');
  });

  afterAll(async () => {
    await serverInstance.close();
    await close();
  });
});