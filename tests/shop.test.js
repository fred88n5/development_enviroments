const request = require('supertest');
const api = 'https://arturomora.com/fsa/products/';

describe('Product Fetching Endpoint', () => {
  it('should fetch products successfully', async () => {
    const res = await request(api)
   .get('')
   .expect(200);
    expect(res.body).toBeInstanceOf(Array);
  });
});
