document.getElementById('frm_login').addEventListener('submit', async function(event) {
    event.preventDefault();

    const email_input = document.getElementById('email');
    const password_input = document.getElementById('password');

    // Email and password input value
    const email = email_input.value;
    const password = password_input.value;

    try {
        // Fetch request to login endpoint
        const response = await fetch('http://192.46.239.197/api/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email, password }),
        });

        // Check response is ok
        if (!response.ok) {
            throw new Error('Invalid email or password');
        }

        // Saving user email in session
        sessionStorage.setItem('user_email', email);
        // Redirect to shop.html
        window.location.href = '/html/shop.html?category=all';
    } catch (error) {
        alert('Invalid email or password');
    }
});
