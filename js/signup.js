const password_validator = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&.])[A-Za-z\d@$!%*?&.,/#+-_()=|;:{}]{8,20}$/;


document.getElementById('frm_signup').addEventListener('submit', async (e)=>{
    e.preventDefault();

    const email_input = document.getElementById('email');
    const password_input = document.getElementById('password');
    const confirm_password_input = document.getElementById('confirm_password');

    // Validate the length of the email
    if(email_input.value.length > 50){
        alert('Email can maximum be 50 characters')
        return false;
    };

    // Validate the password input
    /*if(!password_input.value.match(password_validator)){
            alert('Enter a valid password');
            return false;
        };
*/
    // Validate the password input
    if(password_input.value !== confirm_password_input.value){
        alert('The passwords dont match');
        return false;
    };

    // Create a new user object
    const new_user = {
        email: email_input.value,
        password: password_input.value,
    };

    // Send a POST request to add the new user via try catch
    try {
        const response = await fetch('http://192.46.239.197/api/signup', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(new_user),
        });

        if (response.ok) {
            // If the user is added, redirect to the login page after closing the alert
            window.location.href = '../index.html';
            alert('Successfully signed up! You will now be redirected to the login page!');
        } else {
            alert('Error adding user. Please try again.');
        }
    } catch(error) {
        console.error('Error adding user:', error);
        alert('Error adding user. Please try again.');
    }
});
