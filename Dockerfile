# Use Node.js version 21
FROM node:21

# Set the working directory in the Docker image
WORKDIR /app/

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install the Node.js dependencies
RUN npm install

# Copy the rest of the application to the working directory
COPY . /app/

# Show that we are publishing
EXPOSE 3000

# Start the application
ENTRYPOINT [ "node", "server.js" ]