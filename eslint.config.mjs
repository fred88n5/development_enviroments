export default [
  {
    files: ["**/*.js"],
    languageOptions: { 
      // module type
      sourceType: "commonjs",
    },
    rules: {
       'no-multiple-empty-lines': ['error', { 'max': 2, 'maxBOF': 2 }],
       'no-trailing-spaces': ['error'],
       'eqeqeq': ['error'],
    },
  },
];