require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const { db } = require('./db');
const bcrypt = require('bcrypt');
const cors = require('cors');
const server = express();
const port = process.env.SERVER_PORT || 3000;

server.use(cors());
server.use(bodyParser.json());

let usersCollection;
server.use(async (req, res, next) => {
  if (!usersCollection) {
    const dbConnection = await db();
    usersCollection = dbConnection.users;
  }
  next();
});

const signup = async (req, res) => {
  const { email, password } = req.body;

  // Check if user already exists
  const existingUser = await usersCollection.findOne({ email });
  if (existingUser) {
    return res.status(400).send('A user with this email already exists. Please use a different email.');
  }

  // Hash the password
  const hashedPassword = await bcrypt.hash(password, 10);

  // Create a new user
  const newUser = { email, password: hashedPassword };
  await usersCollection.insertOne(newUser);

  res.status(201).send('User created successfully');
};

const login = async (req, res) => {
  const { email, password } = req.body;

  // Find the user
  const user = await usersCollection.findOne({ email });
  if (!user) {
    return res.status(400).send('Invalid email or password');
  }

  // Check the password
  const validPassword = await bcrypt.compare(password, user.password);
  if (!validPassword) {
    return res.status(400).send('Invalid email or password');
  }

  res.send('Logged in successfully');
};

const fetchUsers = async (req, res) => {
  const users = await usersCollection.find().toArray();
  res.send(users);
};

const deleteUser = async (req, res) => {
  const { email } = req.params;
  await usersCollection.deleteOne({ email });
  res.send('User deleted');
};

const deleteAllUsers = async (req, res) => {
  await usersCollection.deleteMany();
  res.send('All users deleted');
};

server.post('/api/signup', signup);
server.post('/api/login', login);
server.get('/api/users', fetchUsers);
server.delete('/api/users/:email', deleteUser);
server.get('/api/delete', deleteAllUsers);

const serverInstance = server.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});

module.exports = {
  server,
  serverInstance,
  signup,
  login,
  fetchUsers,
  deleteUser,
  deleteAllUsers
};