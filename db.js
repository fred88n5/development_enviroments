const MongoClient = require('mongodb').MongoClient;

require('dotenv').config();

let client;

async function db() {
    const host = process.env.MONGO_HOST;
    const port = process.env.MONGO_PORT;
    const dbName = process.env.NODE_ENV === 'test' ? 'de_exam_test' : process.env.MONGO_DB;
    const uri = `mongodb://${host}:${port}/${dbName}`;

    client = new MongoClient(uri);

    try {
        await client.connect();
        console.log(`Connected to MongoDB at ${dbName}`);

        const database = client.db();
        const users = database.collection('users');

        return { users };
    } catch (err) {
        console.error(err);
    }
}

async function close() {
    try {
        await client.close();
    } catch (error) {
        console.error('Failed to close MongoDB client:', error);
    }
}

module.exports = { db, close };